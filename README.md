# Revanced helper

- If first time, initialize files

```sh
$ make init
```

- If not, just update

```sh
$ make update
```

## Usage

You can run this if you have:

- your keystore as `ReVanced.keystore`
- your apks stored in `APKs/`

<!-- The password is the default of ReVanced Manager -->

Replace patch1 with your patch, and continue...

> [For patches name, use quotes](https://revanced.app/patches)
> You can get the list of patches using:
>   ```sh
>   java -jar revanced-cli.jar list-patches -f <app> "revanced-patches.jar"
>   ```

```sh
java -jar revanced-cli.jar patch \
    --patch-bundle="revanced-patches.jar" \
    --keystore="ReVanced.keystore" \
    --merge="revanced-integrations.apk" \
    --exclusive \
    --out="revanced-app.apk" \
    --alias=alias \
    --keystore-entry-password="s3cur3p@ssw0rd" \
    --keystore-password="s3cur3p@ssw0rd" \
    "APKs/XXX.apk" \
    -i patch1
```
