RVD_CLI         = revanced-cli
RVD_CLI_EXT     = jar
RVD_INT         = revanced-integrations
RVD_INT_EXT     = apk
RVD_PATCHES     = revanced-patches
RVD_PATCHES_EXT = jar

ADB_ARCHIVE = platform-tools-latest-linux.zip

GITHUB_URL      = https://api.github.com/repos
GITHUB_REPO     = revanced
GITHUB_ENDPOINT = releases/latest

EXTENSIONS   = *.jar *.apk
VERSIONS_TXT = versions.txt

WGET  = wget -q --show-progress
CURL  = curl -s
RM    = rm -f
MV    = mv
LS    = ls
ECHO  = echo
UNZIP = unzip

GREP_DOWNLOADS = grep 'browser_'
GREP_LATEST    = grep -v 'dev' | head -1
REMOVE_QUOTES  = cut -d\" -f4

all: build

update_version_file:
	@$(LS) $(EXTENSIONS) > $(VERSIONS_TXT)
	$(MV) $(RVD_CLI)*.$(RVD_CLI_EXT) $(RVD_CLI).$(RVD_CLI_EXT)
	$(MV) $(RVD_INT)*.$(RVD_INT_EXT) $(RVD_INT).$(RVD_INT_EXT)
	$(MV) $(RVD_PATCHES)*.$(RVD_PATCHES_EXT) $(RVD_PATCHES).$(RVD_PATCHES_EXT)
	@$(ECHO) Version file updated!

init: update
init:
	@$(WGET) https://dl.google.com/android/repository/$(ADB_ARCHIVE)
	@$(UNZIP) $(ADB_ARCHIVE)
	@$(RM) $(ADB_ARCHIVE)

update: clean
update: download_cli
update: download_INT
update: download_patches
update: update_version_file
update:
	@$(ECHO) Files updated!

download_cli: GREPS = $(GREP_DOWNLOADS) | grep '$(RVD_CLI_EXT)' | $(GREP_LATEST) | $(REMOVE_QUOTES)
download_cli:
	@$(WGET) $(shell $(CURL) $(GITHUB_URL)/$(GITHUB_REPO)/$(RVD_CLI)/$(GITHUB_ENDPOINT) | $(GREPS))

download_INT: GREPS = $(GREP_DOWNLOADS) | grep '$(RVD_INT_EXT)' | $(GREP_LATEST) | $(REMOVE_QUOTES)
download_INT:
	@$(WGET) $(shell $(CURL) $(GITHUB_URL)/$(GITHUB_REPO)/$(RVD_INT)/$(GITHUB_ENDPOINT) | $(GREPS))

download_patches: GREPS = $(GREP_DOWNLOADS) | grep '$(RVD_PATCHES_EXT)' | $(GREP_LATEST) | $(REMOVE_QUOTES)
download_patches:
	@$(WGET) $(shell $(CURL) $(GITHUB_URL)/$(GITHUB_REPO)/$(RVD_PATCHES)/$(GITHUB_ENDPOINT) | $(GREPS))

clean:
	$(RM) $(EXTENSIONS)

build:
	@$(ECHO) build
